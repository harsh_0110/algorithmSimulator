orient <- function(data){

  ##################################################################################
  ###             Function to detect axis and normalize data points              ###
  ##################################################################################


  ### Matrix definition reqd. to multiply with each tuple
  rotMat <- function(vi) {

    library(Matrix)
    anorm = matrix(c(-(vi[3]/sqrt(vi[1]^2+vi[3]^2)),0,(vi[1]/sqrt(vi[1]^2+vi[3]^2))),nrow=1,ncol=3)
    alpha = acos(vi[2]/sqrt(vi[1]^2+vi[3]^2+vi[2]^2))
    q = c(cos(alpha/2),sin(alpha/2)*anorm[1,1],sin(alpha/2)*anorm[1,2],sin(alpha/2)*anorm[1,3])
    R = matrix(c(1-2*(q[3]^2+q[4]^2),2*(q[2]*q[3]+q[1]*q[4]),2*(q[2]*q[4]-q[1]*q[3]),2*(q[2]*q[3]-q[1]*q[4]),1-2*(q[2]^2+q[4]^2),2*(q[2]*q[1]+q[3]*q[4]),2*(q[1]*q[3]+q[2]*q[4]),2*(q[3]*q[4]-q[1]*q[2]),1-2*(q[2]^2+q[3]^2)),nrow=3,ncol=3)

    return (R)
  }

  
  ### vi is list of data points initially
  vi <- c(data$accnX[1],data$accnY[1],data$accnZ[1])
  R <- rotMat(vi)
  #R <- round(R, 2)


  ### Multiplying each data pont with rotMat
  data$AccelX <- (R[1,1] * data$accnX) + (R[1,2] * data$accnY) + (R[1,3] * data$accnZ)
  data$AccelY <- (R[2,1] * data$accnX) + (R[2,2] * data$accnY) + (R[2,3] * data$accnZ)
  data$AccelZ <- (R[3,1] * data$accnX) + (R[3,2] * data$accnY) + (R[3,3] * data$accnZ)

  data$accnX <- NULL
  data$accnY <- NULL
  data$accnZ <- NULL 
  
  return(data) 
}
