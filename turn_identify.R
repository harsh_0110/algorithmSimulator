turn <- function(data){
  
  winList <- plotWin(data)
  data <- merge(data,winList[,c("Key","endpt_cluster")], all.x = TRUE, by = "Key")
  
  #### Set All NA's in endpt_cluster to 1 for straight road
  data[is.na(data$endpt_cluster),"endpt_cluster"]=1
  
  #### Rash Event Identification
  eventDivision = roadDivision(data)
  return(eventDivision)
}