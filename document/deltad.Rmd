---
title: "deltad"
author: "Abhishek"
date: "Thursday, March 10, 2016"
output: html_document
---

***  
***  

This function is used to calculate distance between every consecutive pair of Latitude and Longitude and storing the distance in new column named deltadist along the latter tuple.

```{r, eval=FALSE}
  deltadist<-as.vector(seq(length.out=length(data$Time_Accel),by=0,0))
```

***  

First we need to create our proper vector column to compute distance, also the values are needed in radian to apply distance formula.

We need to create two vector for each of Latitude and Longitude column.
First vector will contain values from **1** to **n-1** values and other contains **2** to **n** values for both lat and long.

After creating these vector, all we have to do is to subtract former from latter and save this into **deltalat** and **deltalong**.

```{r, eval=FALSE}
  lat1 <- as.vector(data$Latitude*3.14/180)
  lat1 <- lat1[-length(lat1)]
  ...
  ...
  lon2<- as.vector(data$Longitude*3.14/180)
  lon2 <- lon2[-1]
  ...

  deltalat = lat2 - lat1
  deltalong = lon2 - lon1
```


We will use following formula to calculate distance using latitude, longitude points:

```{r,eval=FALSE}
  a = sin(deltalat/2)^2 + cos(lat1)*cos(lat2)*sin(deltalong/2)^2 
  d = 2*atan2(sqrt(a),sqrt(1-a))
```


*** 

Here, we are multiplying this `d` with radius of earth i.e, 6371000 to convert this value into meters.

```{r, eval=FALSE}
  d = 6371000*d
```


*** 
***  

[click here](document.html)to return on main page.

***  
***
